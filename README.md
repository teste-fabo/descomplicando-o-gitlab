# Descomplicando o GitLAB

Treinamento descomplicando o Gitlab criado ao vivo na Twitch

### Day-1

```bash
- Entendemos o que é o Git
- Entendemos o que é Working Dir, Index e HEAD
- Entendemos o que é o Gitlab
- Como criar um Grupo no Gitlab
- Como criar um repositório Git
- Aprendemos os comandos básicos para manipução de arquivos e diretórios no git
- Como criar uma branch
- Como criar um Merge Request
- Como adicionar um Membro no projeto
- Como fazer o merge na Master/Main
- Como associar um repo local com um repo remoto
- Como importar um repo do GitHub para o Gitlab
- Mudamos a branch padrão para Main
```
